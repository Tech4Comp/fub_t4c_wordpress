<?php
//* Code goes here
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

/*
 * Moved from the FetchMatch template file on
 * 30.11.2020 ~11:??
 */
function fetch_match_pdo() {
	$servername = "tech4comp.dbis.rwth-aachen.de:31029"; // Updated 30.11.2020 09:54
	$current_user = wp_get_current_user(); // Added 30.11.2020 09:53 - thanks Steven S. and Antony on https://wordpress.stackexchange.com/questions/49686/how-do-i-display-logged-in-username-if-logged-in
	$username = $current_user->user_login; // Updated 30.11.2020 09:54 - thanks Steven S. and Antony on https://wordpress.stackexchange.com/questions/49686/how-do-i-display-logged-in-username-if-logged-in
	$password = strrev($username); // Updated 30.11.2020 09:56 - strrev is a php String reverse function

	try {
	$conn = new PDO("mysql:host=$servername;dbname=Matching_Tool_database", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "Connected successfully";
	} catch(PDOException $e) {
	echo "Connection failed: " . $e->getMessage();
	}


	/*
	 * HAD A TERRIBLE BUG - USED $pdo instead of $conn BECAUSE I TOOK THE
	 * TEMPLATE FORM SOMEWHERE..
	 * FIXED 30.11.2020 12:34
	 */
	$stmt = $conn->prepare('SELECT * FROM `Matching_Tool_database`.`:user match`'); // Added 30.11.2020 10:07 - thanks https://phpdelusions.net/pdo
	$stmt->execute(['user' => $username]); // Added 30.11.2020 10:07 - thanks https://phpdelusions.net/pdo
	$match = $stmt->fetch(); // Added 30.11.2020 10:18 - thanks https://phpdelusions.net/pdo

echo "$match";
}

/*
 * 01.12.2020 10:38
 * I hope this works...
 * using mysqli
 */
function fetch_match() {
	$servername = "tech4comp.dbis.rwth-aachen.de:31029"; 
	$current_user = wp_get_current_user(); 
	$username = $current_user->user_login; 
	$password = strrev($username);

	//try {
	$conn = new mysqli($servername, $username, $password, "Matching_Tool_database");
	//echo nl2br("Connected successfully\n"); //DEBUG
	//} catch(Exception $e) {
	//echo "Connection failed: " . $e->getMessage();
	//}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:13
	 */
	if ($conn->connect_errno) {
		echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
	}

	//$stmt = $conn->prepare("SELECT * FROM `Matching_Tool_database`.`? match`"); 
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:14
	 */
	if (!($stmt = $conn->prepare("SELECT * FROM `Matching_Tool_database`.`".$username." match`"))) {
		echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
	}		
	//echo nl2br("prepared\n"); //DEBUG
	//$stmt->bind_param("s", $name); // 01.12.2020 12:15 - apparently the first parameter here represents the type.. so 's' for String I guess
	//$name = $username;
	//echo("bound ". $username. " to user"); //DEBUG
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:15
	 */
	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	//echo nl2br("\nexecuted\n"); //DEBUG
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:16
	 */
	if (!($result = $stmt->get_result())) {
		echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	while ($row = $result->fetch_array(MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                //print "$r ";
				return $r;
            }
            print "\n";
        }
	//echo $res->Match;
	//$match = $res->fetch_assoc();
	//var_dump($res->fetch_all());
	//foreach ( $res as $result ) { 
	//print($result->Match);
	//echo '<script>console.log("A MATCH: ' . $result->Match . '")</script>';
	//echo  $result->name;
	//}
	$stmt->close();
	$conn->close();
	//echo "$match";
}

/*
 * 01.04.2021 12:09
 */
function fetch_match_clean() {
	$servername = "tech4comp.dbis.rwth-aachen.de:31029"; 
	//$servername = "tech4comp.dbis.rwth-aachen.de";
	//$port = 31029; 
	$current_user = wp_get_current_user(); 
	$username = $current_user->user_login;
	/*
	 * Since usernames for MySQL can be up to 32 characters long, I must trim them to this size here as well (13:36)
	 * (13:51) take the min between length and 32.
	 */
	$untrimmed_mail = $current_user->user_email;
	$end_index = min(strlen($untrimmed_mail), 32);
	$usermail = strval(substr($untrimmed_mail,0,32)); // strval did the trick 06.04.2021 15:42
	//$usermail = substr(htmlentities($untrimmed_mail),0,end_index);
	$password = strrev($usermail);
	/*echo $untrimmed_mail.PHP_EOL;
	echo strlen($untrimmed_mail).PHP_EOL;
	echo $end_index.PHP_EOL;*/
	/*echo nl2br($usermail."\n");
	echo strlen($untrimmed_mail);
	echo strlen($usermail);
	echo $password.";";
	echo $username.";";*/
	$conn = new mysqli($servername, $usermail, $password, "Matching_Tool_database");
	//$conn = new mysqli($servername, $usermail, $password, "Matching_Tool_database", $port);
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:13
	 */
	if ($conn->connect_errno) {
		echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
	}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:14
	 ~~~~~~~~~~~~~~~~~~~~~~~~
	 * With user mail for the match name
	 * is more robust
	 * 07.04.2021 08:57
	 */
	if (!($stmt = $conn->prepare("SELECT * FROM `Matching_Tool_database`.`".$username." match`"))) {
		echo "2";
	}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:15
	 */
	if (!$stmt->execute()) {
		echo "3";
	}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:16
	 */
	if (!($result = $stmt->get_result())) {
		echo "4";
	}
	while ($row = $result->fetch_array(MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                return $r;
            }
            print "\n";
        }
	$stmt->close();
	$conn->close();
}

/*
 * 06.05.2021 14:06 -
 * Using a new approach:
 * the central admin view with a stored procedure
 * that I've made that first checks if the user is peer
 * or mentor/mentee, and then calls an appropriate
 * stored procedure for each case that searches
 * the appropriate match according to user email
 * and returns the name of that user's match 
 * or No match found
 */
function fetch_match_central_view() {
	$servername = "tech4comp.dbis.rwth-aachen.de:31029"; 
	$current_user = wp_get_current_user(); 
	$username = "tech4comp";
	$password = strrev($username);
	$usermail = $current_user->user_email;
	$conn = new mysqli($servername, $username, $password, "Matching_Tool_database");
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:13
	 */
	if ($conn->connect_errno) {
		echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
	}
	/*
	 * 07.05.2021 09:58 thanks https://www.php.net/manual/en/mysqli.quickstart.stored-procedures.php
	 */
	if (!($stmt = $conn->prepare("SET @matched = ''"))) {
		echo "2a";
	}
	if (!$stmt->execute()) {
		echo "3a - SET @matched = '' FAILED";
	}
	if (!($stmt = $conn->prepare("CALL FindAnyMatch('".$usermail."', @matched, @score)"))) {
		echo "2b";
	}
	if (!$stmt->execute()) {
		echo "3b - CALL FindAnyMatch('".$usermail."', @matched, @score) FAILED";
	}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:14
	 */
	if (!($stmt = $conn->prepare("select @matched"))) {
		echo "2c";
	}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:15
	 */
	if (!$stmt->execute()) {
		echo "3c";
	}
	/*
	 * Thanks php.net
	 * for the error handling examples
	 * 02.12.2020 09:16
	 */
	if (!($result = $stmt->get_result())) {
		echo "4";
	}
	while ($row = $result->fetch_array(MYSQLI_NUM))
        {
            foreach ($row as $r)
            {
                return $r;
            }
            print "\n";
        }
	$stmt->close();
	$conn->close();
}

/*
 * 01.12.2020 13:56 -
 * Ended up opting for a 
 * short code instead of
 * a template.
 * 14:21 - IT WORKED!
 */
add_shortcode('match', 'fetch_match_central_view');

/*
 * 01.12.2020 13:56 -
 * Now also a shortcode for the logged in
 * user name
 */
function get_logged_in_user() {
	$current_user = wp_get_current_user();
	return $current_user->user_login;
}
/*
 * 01.12.2020 14:22 - 
 * short code for the logged-in-username
 */
 add_shortcode('print_username', 'get_logged_in_user');
  
 /*
 * 15.03.2021 10:25
 */
 function import_survey() {
	 /*
 * 15.03.2021 10:24
 */
 header('Access-Control-Allow-Origin: https://limesurvey.tech4comp.dbis.rwth-aachen.de');
	 ?>

<link rel="import" href="https://limesurvey.tech4comp.dbis.rwth-aachen.de/index.php/175579?newtest=Y&lang=de">

<?php
 }
 add_shortcode('survey', 'import_survey');
 
 /**
  * 01.09.2021 13:44
  * 
  */
 function retrieve_user_matching() {
	$current_user = wp_get_current_user();
	$usermail = $current_user->user_email;
	$base_url = 'http://mmmt-dev.tech4comp.dbis.rwth-aachen.de';
	$response_body = wp_remote_retrieve_body( wp_remote_get( $base_url.'/api/produce_matching_user?email='.$usermail ));
	echo $response_body;
 }
 
 add_shortcode('retrieve_user_matching', 'retrieve_user_matching');